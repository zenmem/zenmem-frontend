FROM node:latest
MAINTAINER Volker Spaarmann spaarmann@uni-paderborn.de
RUN mkdir -p /zenmem /home/nodejs && \
        groupadd -r nodejs && \
        useradd -r -g nodejs -d /home/nodejs -s /sbin/nologin nodejs && \
        chown -R nodejs:nodejs /home/nodejs

WORKDIR /zenmem
COPY . /zenmem
RUN npm install && npm run postinstall
RUN chown -R nodejs:nodejs /zenmem
USER nodejs
CMD npm start

EXPOSE 5555