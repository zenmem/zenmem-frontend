import { join } from 'path';

import { SeedConfig } from './seed.config';

/**
 * This class extends the basic seed configuration, allowing for project specific overrides. A few examples can be found
 * below.
 */
export class ProjectConfig extends SeedConfig {

  PROJECT_TASKS_DIR = join(process.cwd(), this.TOOLS_DIR, 'tasks', 'project');

  constructor() {
    super();
    // this.APP_TITLE = 'Put name of your app here';

    /* Enable typeless compiler runs (faster) between typed compiler runs. */
    // this.TYPED_COMPILE_INTERVAL = 5;

    // Add `NPM` third-party libraries to be injected/bundled.
    this.NPM_DEPENDENCIES = [
        ...this.NPM_DEPENDENCIES,
        { src: 'jquery/dist/jquery.min.js', inject: 'libs' },
        { src: 'tether/dist/js/tether.min.js', inject: 'libs' },
        { src: 'bootstrap/dist/css/bootstrap.css', inject: true },
        { src: 'bootstrap/dist/js/bootstrap.min.js', inject: 'libs' },
        { src: 'components-font-awesome/css/font-awesome.min.css', inject: true },
        { src: 'codemirror/lib/codemirror.css', inject: true },
        { src: 'codemirror/theme/neo.css', inject: true },
        { src: 'codemirror/addon/dialog/dialog.css', inject: true },
        { src: 'vkbeautify/index.js', inject: 'libs' },
        { src: 'underscore/underscore.js', inject: 'libs' },
        { src: 'codemirror/lib/codemirror.js', inject: 'libs' },
        { src: 'codemirror/mode/scheme/scheme.js', inject: 'libs' },
        { src: 'codemirror/mode/xml/xml.js', inject: 'libs' },
        { src: 'codemirror/addon/dialog/dialog.js', inject: 'libs' },
        { src: 'codemirror/addon/search/searchcursor.js', inject: 'libs' },
        { src: 'codemirror/addon/search/search.js', inject: 'libs' },
        { src: 'codemirror/addon/search/jump-to-line.js', inject: 'libs' },
        { src: 'file-saver/FileSaver.min.js', inject: 'libs' },
        { src: 'd3/build/d3.min.js', inject: 'libs' },
        { src: 'swagger-client/browser/swagger-client.js', inject: 'libs' }
      // {src: 'lodash/lodash.min.js', inject: 'libs'},
    ];

    // Add `local` third-party libraries to be injected/bundled.
    this.APP_ASSETS = [
      ...this.APP_ASSETS,
      // {src: `${this.APP_SRC}/your-path-to-lib/libs/jquery-ui.js`, inject: true, vendor: false}
      // {src: `${this.CSS_SRC}/path-to-lib/test-lib.css`, inject: true, vendor: false},
    ];

    /* Add to or override NPM module configurations: */
     this.mergeObject(this.PLUGIN_CONFIGS['browser-sync'], { ghostMode: false });
  }

}
