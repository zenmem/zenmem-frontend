ZenMEM Frontend

In order to start the zenmem-frontend use:

```bash
git clone --depth 1 https://bitbucket.org/zenmem/zenmem-frontend.git
cd zenmem-frontend
# install the project's dependencies
npm install
#run the project
npm start
```