﻿import { Component } from '@angular/core';
import { REQUEST, MODE_NONE, MODE_EDITOR, MODE_PREVIEW } from './types/constants';
import { DirtyPart } from './types/dirtyPart';
import { Alternative } from './types/alternative';
import { XmlNamespaceDeclaration } from './types/xmlNamespaceDeclaration';
import { HistoryEvent } from './types/historyEvent';

import { FileComponent } from './file.component';
import { XMLService } from './services/xml.service';
import { StringService } from './services/string.service';
import { SeditorService } from './services/seditor.service';
import { CodeMirrorDirective } from './directives/codemirror.directive';
import { LocalStorageService, SessionStorageService } from 'ng2-webstorage';



declare var _: any;
declare var saveAs: any;

@Component({
    moduleId: module.id,
    selector: "seditor",
    templateUrl: "seditor.html"

})
export class SEditorComponent {
    request: string[];
    dirtyParts: DirtyPart[];
    page: number;
    paginator: number;
    output: string;
    mode: string;
    canUndo: boolean;
    selectionExists: boolean;
    held: boolean;
    history: HistoryEvent[];
    // Href to blob that contains exported data
    // DOM reprasentation of imported MEI file
    xmlDoc: Document;
    // Array of found xml-namespace declarations
    xmlNamespaceDeclarations: XmlNamespaceDeclaration[];
    blobHref: string;
    fileName: string;

    restoreData: boolean = true;
    shistorylength: number;


    constructor(private xmlService: XMLService, private stringService: StringService, private seditorService: SeditorService,
        private localStorage: LocalStorageService,
        private sessionStorage: SessionStorageService) {
        this.request = REQUEST;
        this.dirtyParts = [];
        this.page = 0;
        this.paginator = 0;
        this.output = "";
        this.mode = MODE_NONE;
        this.canUndo = false;
        this.selectionExists = false;
        this.history = [];
        this.blobHref = "";
        this.xmlNamespaceDeclarations = [];    
    }

    restore(): void {
        this.dirtyParts = this.sessionStorage.retrieve('sdirtyParts');
        if (this.dirtyParts === null)
        {
            alert("There is no data to restore");
            this.restoreData = false;
            this.dirtyParts = [];
                     
        }
        else
        {
            this.restoreData = true;
            this.page = this.localStorage.retrieve('spage');
            this.paginator = this.localStorage.retrieve('spaginator');
            this.shistorylength = this.localStorage.retrieve('shistorylength');
            if (this.shistorylength === 0) {
                this.canUndo = false;
            }
            this.selectionExists = true;
            this.mode = 'EDITOR'; 
        } 
          
    };

    import(data: any): void {
        var oParser: DOMParser = new DOMParser();
        var pureXMLString: string = this.xmlService.hideXMLNamespaceDeclarations(data.fileContent, this.xmlNamespaceDeclarations);
        this.fileName = data.fileName;
        var oDOM: Document = oParser.parseFromString(pureXMLString, "text/xml");
        this.xmlService.clean(oDOM);
        this.xmlDoc = oDOM;
      
        var occurences: Node[] = this.xmlService.searchOccurrences(this.xmlDoc, this.request);
        this.page = 0;
        this.paginator = 0;
        this.dirtyParts = this.seditorService.processDirtyXML(this.xmlDoc, occurences);
        this.restoreData = true;
        this.mode = 'EDITOR';
    }

    export(): void {
        this.output = this.seditorService.calculateOutput(this.dirtyParts, true, this.xmlNamespaceDeclarations);
        var blob = new Blob([this.output], { type: 'text/xml' });
        saveAs(blob, this.fileName);
    };

    hold(): void {
        this.history.push(new HistoryEvent(
            _.map(this.dirtyParts, _.clone) as DirtyPart[], this.page));

        var lastHistoryEvent: HistoryEvent = this.history.pop();
        this.sessionStorage.store('sdirtyParts', lastHistoryEvent.dirtyParts);
        this.localStorage.store('shistorylength', this.history.length);
        this.localStorage.store('spage', lastHistoryEvent.page);
        this.localStorage.store('spaginator', lastHistoryEvent.page);

      
        //needsRecalculation needs for recursiv Alternatives
        var needsRecalculation: boolean = false;
        var changed: boolean = false;
        for (var i = 0; i < this.dirtyParts.length - 1; i++) {
            var current: DirtyPart = this.dirtyParts[i];
            var next: DirtyPart = this.dirtyParts[i + 1];

            if (current.selection !== undefined) {
                changed = true;
                var axml: Node = this.xmlService.StringToXML(current.alternatives[current.selection].code);
               
                var occurences: Node[] = this.xmlService.searchOccurrences(axml, this.request);
               
                if (occurences.length > 0) {
                    needsRecalculation = true;
                }
                var lastContextLine: string = this.stringService.getLastLineOfString(current.context);
                var offset: string = this.stringService.getLineIndents(lastContextLine);
                var indent: string = "    ";
                next.context = current.context +
                    this.stringService.indentString(current.alternatives[current.selection].content, offset, indent, 1) +
                    next.context;
                this.dirtyParts.splice(i--, 1);
            }
        }

        //by recursive occurences calculate the output again
        if (needsRecalculation) {
            var output = this.seditorService.calculateOutput(this.dirtyParts, false, this.xmlNamespaceDeclarations);
            var doc = this.xmlService.StringToXML(output);
            var occurences = this.xmlService.searchOccurrences(doc, this.request);
            this.dirtyParts = this.seditorService.processDirtyXML(doc, occurences);
        }

        if (changed) {
            this.page = 0;
            this.paginator = 0;
        }
        this.selectionExists = false;
        this.held = true;
        this.canUndo = true;
        this.focusAlt();
    };

    undo(): void {
        var lastHistoryEvent: HistoryEvent = this.history.pop();
        this.dirtyParts = lastHistoryEvent.dirtyParts;
        this.page = lastHistoryEvent.page;
        this.paginator = lastHistoryEvent.page;
        if (this.history.length === 0) {
            this.canUndo = false;
        }
        this.selectionExists = true;
        this.focusNav();
    };

    unhold(): void {
        this.dirtyParts = _.map(this.history[0].dirtyParts, _.clone);
        this.history = [];
        this.page = 0;
        this.paginator = 0;
        this.canUndo = false;
        this.selectionExists = true;
        this.focusAlt();
    };

    /**
     * Prepare for preview mode
     */
    previewMode(): void {
        this.output = this.seditorService.calculateOutput(this.dirtyParts, true, this.xmlNamespaceDeclarations);
    };

    /**
             * navigation: show previous page
             */
    previous(): void {
        if (this.page > 0) {
            this.page--;
            this.paginator = this.page;
        }
    };

    /**
     * navigation: show first page
     */
    gotoBegin(): void {
        this.page = 0;
        this.paginator = this.page;
    };

    /**
     * navigation: show last page
     */
    gotoEnd(): void {
        this.page = this.dirtyParts.length - 1;
        this.paginator = this.page;
    };

    /**
     * navigation: show next page
     */
    next(): void {
        if (this.page < this.dirtyParts.length - 1) {
            this.page++;
            this.paginator = this.page;         
        }

    };

    /**
     * navigation: go to specific page
     * @param {number} page is number of the page, which should be shown
     */
    gotoPage(page: number): void {
        if (page >= 0 && page < this.dirtyParts.length) {
            this.page = page;
        }
    };

    /**
     * scroll to alternative
     */
    focusAlt(): void {
        // set the location.hash to the id of
        // the element you wish to scroll to.
        //$location.hash('altAnchor');

        // call $anchorScroll()
        //$anchorScroll();
    };

    /**
     * scroll to panel header
     */
    focusMenu(): void {
        // set the location.hash to the id of
        // the element you wish to scroll to.
        //$location.hash('menuAnchor');

        // call $anchorScroll()
        //$anchorScroll();
    };

    /**
     * scroll to panel footer
     */
    focusNav(): void {
        // set the location.hash to the id of
        // the element you wish to scroll to.
        //$location.hash('navAnchor');

        // call $anchorScroll()
        //$anchorScroll();
    };

    checkSelections(dirtyParts: DirtyPart[]): boolean {
        var result: boolean = false;
        dirtyParts.forEach(dp => result = (dp.selection !== undefined) || result);
        return result;
    }

    /**
     * trigger the selectionExists flag and scroll to panel footer
     */
    selection(event: Event): void {
        if (this.dirtyParts[this.page].selection === undefined) {
            this.selectionExists = this.checkSelections(this.dirtyParts);
        } else {
            this.selectionExists = true;
            this.focusNav();
        }
    };
}