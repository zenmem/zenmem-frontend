﻿import { Directive, HostListener, SimpleChange, Input, OnInit, ElementRef, OnChanges, AfterViewInit } from '@angular/core';
import { StringService } from '../services/string.service';
import { CM_ALTERNATIVE, CM_CONTEXT, CM_PREVIEW, CM_GENERAL, cmXScrollingHeight, cmCodeLineHeight } from '../types/constants';

@Directive({
    selector: 'codemirror',
    host: {
        '(window:resize)': 'onResize($event.target)'
    }

})
export class CodeMirrorDirective implements OnInit, OnChanges, AfterViewInit {

    editor: any;
    editorNativeElement: any;
    @Input() value: string;
    @Input() usage: string;
    cmContextMaxHeight: number;
    cmPreviewMaxHeight: number;
    cmGeneralMaxHeight: number;

    onResize(win: any) {
        this.cmContextMaxHeight = win.innerHeight - 350;
        this.cmPreviewMaxHeight = win.innerHeight - 195;
        this.cmGeneralMaxHeight = win.innerHeight - 400;

        var linesCount: number = this.stringService.countLines(this.value);
        var cmAutoSize = cmXScrollingHeight + linesCount * cmCodeLineHeight;

        switch (this.usage) {
            case CM_ALTERNATIVE:
                this.editor.setSize('inherit', cmAutoSize);
                break;
            case CM_CONTEXT:
                this.editor.setSize('inherit', Math.min(cmAutoSize, this.cmContextMaxHeight));
                break;
            case CM_PREVIEW:
                this.editor.setSize('inherit', Math.min(cmAutoSize, this.cmPreviewMaxHeight));
                break;
            default:
                this.editor.setSize('inherit', Math.min(cmAutoSize, this.cmGeneralMaxHeight));
                break;
        }
    }

    constructor(elRef: ElementRef, private stringService: StringService) {
        this.editorNativeElement = elRef.nativeElement;
        if (this.usage === undefined) {
            this.usage = CM_GENERAL;
        }
        this.cmContextMaxHeight = window.innerHeight - 350;
        this.cmPreviewMaxHeight = window.innerHeight - 195;
        this.cmGeneralMaxHeight = window.innerHeight - 400;
    }

    ngOnInit() {
    }

    ngAfterViewInit() {
        this.editor = CodeMirror(this.editorNativeElement, {
            lineNumbers: true,
            mode: "xml",
            value: this.value,
            readOnly: true,
            theme: "neo"
        });

        var linesCount: number = this.stringService.countLines(this.value);
        var cmAutoSize = cmXScrollingHeight + linesCount * cmCodeLineHeight;

        switch (this.usage) {
            case CM_ALTERNATIVE:
                this.editor.setSize('inherit', cmAutoSize);
                break;
            case CM_CONTEXT:
                this.editor.setSize('inherit', Math.min(cmAutoSize, this.cmContextMaxHeight));
                break;
            case CM_PREVIEW:
                this.editor.setSize('inherit', Math.min(cmAutoSize, this.cmPreviewMaxHeight));
                break;
            default:
                this.editor.setSize('inherit', Math.min(cmAutoSize, this.cmGeneralMaxHeight));
                break;
        }

        this.editor.on('change', (editor: CodeMirror.Editor) => {
            var value = this.editor.getDoc().getValue();
        });
    }

    ngOnChanges(changes: { [propName: string]: SimpleChange }) {
        if (this.editor !== undefined) {
            this.editor.getDoc().setValue(this.value);
            var linesCount: number = this.stringService.countLines(this.value);
            var cmAutoSize = cmXScrollingHeight + linesCount * cmCodeLineHeight;

            switch (this.usage) {
                case CM_ALTERNATIVE:
                    this.editor.setSize('inherit', cmAutoSize);
                    break;
                case CM_CONTEXT:
                    this.editor.setSize('inherit', Math.min(cmAutoSize, this.cmContextMaxHeight));
                    break;
                case CM_PREVIEW:
                    this.editor.setSize('inherit', Math.min(cmAutoSize, this.cmPreviewMaxHeight));
                    break;
                default:
                    this.editor.setSize('inherit', Math.min(cmAutoSize, this.cmGeneralMaxHeight));
                    break;
            }
        }
    }
}