﻿import { SEditorComponent } from './seditor.component';

export const SEditorRoutes = [
    { path: 'seditor', component: SEditorComponent }
];
