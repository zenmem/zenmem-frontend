﻿import {Output, Component, EventEmitter } from '@angular/core';

@Component({
    selector: 'fileservice',
    template: '<input type="file" (change)="changeListener($event)" accept=".xml, .txt, .*" />'
})

export class FileComponent {
    @Output() complete: EventEmitter<any>= new EventEmitter();
    constructor() {
    }

    resultSet: any; // dont need it 
    changeListener($event: any) {
        var self = this;
        var file: File = $event.target.files[0];
        var myReader: FileReader = new FileReader();
        myReader.readAsText(file);
        //var resultSet = [];
        myReader.onloadend = function (e) {
            //self.resultSet = myReader.result; // probably dont need to do this atall
            self.complete.next({
                fileContent: myReader.result,
                fileName: file.name
            }); // pass along the data which whould be used by the parent component
        };
    }
}
