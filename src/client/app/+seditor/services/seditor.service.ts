﻿import { Injectable } from '@angular/core';
import { XMLService } from './xml.service';
import { StringService } from './string.service';
import { DirtyPart } from '../types/dirtyPart';
import { Alternative } from '../types/alternative';
import { XmlNamespaceDeclaration } from '../types/xmlNamespaceDeclaration';
declare var vkbeautify: any;

@Injectable()
export class SeditorService {

    vkb: any;

    constructor(private xmlService: XMLService, private stringService: StringService) { this.vkb = new vkbeautify(); }

    calculateOutput(dirtyParts: DirtyPart[], restoreNS: boolean, xmlNamespaceDeclarations: XmlNamespaceDeclaration[]): string {
        var output = "";
        for (var i = 0; i < dirtyParts.length; i++) {
            output += dirtyParts[i].context;
            if (i === dirtyParts.length - 1) { break; }
            var j = dirtyParts[i].selection;
            if (j !== undefined) {
                output += dirtyParts[i].alternatives[j].content;
            } else {
                output += dirtyParts[i].code;
            }
        }

        if (restoreNS) {
            output = this.xmlService.restoreXMLNamespaceDeclarations(output, xmlNamespaceDeclarations);
        }
        return this.vkb.xml(output);
    }

    processDirtyXML(doc: Node, nodes: Node[]): DirtyPart[] {
        var dirty: DirtyPart[] = [];
        var docStr: string = this.xmlService.XMLToString(doc);
        if (nodes.length === 0) {
            dirty.push(new DirtyPart(null, "", "", this.vkb.xml(docStr), 0, 0, null));
            return dirty;
        }

        var docStrWithAnchors: string = docStr;
        var altList: Alternative[][] = [];
        for (var i = 0; i < nodes.length; i++) {
            var alternatives: Alternative[] = [];
            var children = nodes[i].childNodes;
            for (var j = 0; j < children.length; j++) {
                var child = children[j];
                if (child.nodeName !== '#text') {
                    var content = "";
                    for (var z = 0; z < child.childNodes.length; z++) {
                        content += this.xmlService.XMLToString(child.childNodes[z]);
                    }
                    content = this.vkb.xml(content);
                    alternatives.push(new Alternative(this.vkb.xml(this.xmlService.XMLToString(child)),
                        child.nodeName, content));
                }
            }
            altList.push(alternatives);
            var current = this.xmlService.XMLToString(nodes[i]);
            docStrWithAnchors = this.stringService.replaceSubstringWithAnchor(docStrWithAnchors, current, "<alternatives" + i + "/>");
        }

        docStrWithAnchors = this.vkb.xml(docStrWithAnchors);

        for (var i = 0; i < nodes.length; i++) {
            var context = "";
            if (i === 0) {
                context = docStrWithAnchors.substring
                    (0, docStrWithAnchors.indexOf("<alternatives" + i + "/>"));
            } else {
                context = docStrWithAnchors.substring
                    (docStrWithAnchors.indexOf("<alternatives" + (i - 1) + "/>") +
                    ("<alternatives" + (i - 1) + "/>").length, docStrWithAnchors.indexOf("<alternatives" + i + "/>"));
            }
            dirty.push(new DirtyPart(nodes[i], this.vkb.xml(this.xmlService.XMLToString(nodes[i])),
                nodes[i].nodeName, context, i, altList[i].length, altList[i]));
        }

        dirty.push(new DirtyPart(null, "", "",
            docStrWithAnchors.substring
                (docStrWithAnchors.indexOf("<alternatives" + (nodes.length - 1) + "/>") +
                ("<alternatives" + (nodes.length - 1) + "/>").length, docStrWithAnchors.length),
            i + 1, 0, null));

        return dirty;
        
    };
}