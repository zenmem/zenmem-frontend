﻿import { Injectable } from '@angular/core';
import { xmlNSTagMatchRE, xmlNSReplaseRE } from '../types/constants';
import { XmlNamespaceDeclaration } from '../types/xmlNamespaceDeclaration';
declare var _: any;
declare var vkbeautify: any;

@Injectable()
export class XMLService {
    XMLToString(oXML: Node): string {
        return (new XMLSerializer()).serializeToString(oXML);
    }

    StringToXML(oString: string): Node {
        return (new DOMParser()).parseFromString(oString, "text/xml");
    };

    clean(node: Node): void {
        for (var n = 0; n < node.childNodes.length; n++) {
            var child = node.childNodes[n];
            if (
                //child.nodeType === 8 //COMMENT_NODE ||     //---uncomment this line to ignore xml comments in mei
                (child.nodeType === 3 && !/\S/.test(child.nodeValue)) //TEXT_NODE with indents
            ) {
                node.removeChild(child);
                n--;
            } else if (child.nodeType === 1) { //ELEMENT_NODE
                this.clean(child);
            }
        }
    };

    searchOccurrences(node: Node, request: string[]): Node[] {
        var result:Node[] = [];
        if (_.contains(request, node.nodeName)) {
            result.push(node);
        } else {
            var childNodes: NodeList = node.childNodes;
            for (var i = 0; i < childNodes.length; i++) {
                result = result.concat(this.searchOccurrences(childNodes[i], request));
            }
        }
        return result;
    };

    hideXMLNamespaceDeclarations(xmlString: string, nsDeclarations: XmlNamespaceDeclaration[]): string {
        nsDeclarations.splice(0, nsDeclarations.length);
        var match: any;
        while (match = xmlNSTagMatchRE.exec(xmlString)) {
            var tagName: string = match[1];
            var replased: string[] = match[0].match(xmlNSReplaseRE);
            var tag: string = match[0];
            nsDeclarations.push(new XmlNamespaceDeclaration(tagName, replased, tag));
        }
        return xmlString.replace(xmlNSReplaseRE, "");
    };

    restoreXMLNamespaceDeclarations(xmlString: string, nsDeclarations: XmlNamespaceDeclaration[]): string {
        nsDeclarations.forEach(function (elem) {
            var xmlTagMatchRE: RegExp = new RegExp("<" + elem.tagName + "\\s*[^>]*>");
            xmlString = xmlString.replace(xmlTagMatchRE, elem.tag);
        });

        return xmlString;
    };
}