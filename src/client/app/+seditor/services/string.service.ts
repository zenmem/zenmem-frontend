﻿import { Injectable } from '@angular/core';
import { matchLineIndentSpaces } from '../types/constants';

@Injectable()
export class StringService {
    constructor() { }

    getLastLineOfString(str: string): string {

        var lines: string[] = str.split(/\r?\n/);
        return lines[lines.length - 2];
    }

    countLines(str: string): number {
        var lines: string[] = str.split(/\r?\n/);
        return lines.length;
    }

    getLineIndents(str: string): string {

        return matchLineIndentSpaces.exec(str)[0];
    }

    indentString(str: string, offset: string, indent: string, start: number): string {
        var lines: string[] = str.split(/\r?\n/);
        var result: string = "";
        for (var i = 0; i < start; i++) {
            result += lines[i] + indent + "\n";
        }
        for (var i = start; i < lines.length - 1; i++) {
            result += offset + indent + lines[i] + "\n";
        }
        result += offset + indent + lines[lines.length - 1];
        return result;
    }

    replaceSubstringWithAnchor(str: string, substr: string, anchor: string): string {
        return str.substring(0, str.indexOf(substr)) + anchor + str.substring(str.indexOf(substr) + substr.length, str.length);
    }

    getCodeMirrorNative(target: any) {
        var _target = target;
        if (typeof _target === 'string') {
            _target = document.querySelector(_target);
        }
        if (_target === null || !_target.tagName === undefined) {
            throw new Error('Element does not reference a CodeMirror instance.');
        }

        if (_target.className.indexOf('CodeMirror') > -1) {
            return _target.CodeMirror;
        }

        if (_target.tagName === 'TEXTAREA') {
            return _target.nextSibling.CodeMirror;
        }

        return null;
    };
}