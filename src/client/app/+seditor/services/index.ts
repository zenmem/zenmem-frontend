/**
 * This barrel file provides the export for the shared FooterComponent.
 */
export * from './seditor.service';
export * from './string.service';
export * from './xml.service';