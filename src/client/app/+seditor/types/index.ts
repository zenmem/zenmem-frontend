/**
 * This barrel file provides the export for the shared FooterComponent.
 */
export * from './alternative';
export * from './constants';
export * from './dirtyPart';
export * from './historyEvent';
export * from './xmlNamespaceDeclaration';