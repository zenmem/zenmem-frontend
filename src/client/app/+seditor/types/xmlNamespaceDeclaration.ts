﻿export class XmlNamespaceDeclaration {
    tagName: string;
    replased: string[];
    tag: string;
    constructor(tagName: string, replased: string[], tag: string) {
        this.tagName = tagName;
        this.tag = tag;
        this.replased = replased;
    }
}