﻿export var REQUEST: string[] = [
    'app', 'choice', 'subst'
];

export var xmlNSTagMatchRE: RegExp = /<(\w+)\s+[^>]*(?:xmlns)[^>]*>/g;
export var xmlNSReplaseRE: RegExp = /xmlns(?::\w+)?\s*="[^"]*"/g;
export var matchLineIndentSpaces: RegExp = /^\s+/;

export var MODE_NONE: string = 'NONE';
export var MODE_EDITOR: string = 'EDITOR';
export var MODE_PREVIEW: string = 'PREVIEW';

export var CM_ALTERNATIVE: string = 'ALTERNATIVE';
export var CM_CONTEXT: string = 'CONTEXT';
export var CM_GENERAL: string = 'GENERAL';
export var CM_PREVIEW: string = 'PREVIEW';

export var cmCodeLineHeight: number = 20;
export var cmXScrollingHeight: number = 24;