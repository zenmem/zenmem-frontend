﻿import {Alternative} from './alternative';

export class DirtyPart {
    node: Node;
    code: string;
    tag: string;
    context: string;
    selection: number;
    index: number;
    count: number;
    alternatives: Alternative[];

    constructor(node: Node, code: string, tag: string, context: string, index: number, count: number, alternatives: Alternative[]) {
        this.node = node;
        this.code = code;
        this.tag = tag;
        this.context = context;
        this.index = index;
        this.count = count;
        this.alternatives = alternatives;
        this.selection = undefined;
    }
}