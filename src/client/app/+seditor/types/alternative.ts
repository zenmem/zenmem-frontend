﻿export class Alternative {
    code: string;
    tag: string;
    content: string;
    selected: boolean;
    constructor(code: string, tag: string, content: string) {
        this.code = code;
        this.tag = tag;
        this.content = content;
        this.selected = false;
    }
}