﻿import {DirtyPart} from './dirtyPart';

export class HistoryEvent {
    dirtyParts: DirtyPart[];
    page: number;

    constructor(dirtyParts: DirtyPart[], page: number) {
        this.dirtyParts = dirtyParts;
        this.page = page;
    }
}