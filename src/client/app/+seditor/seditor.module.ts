﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ButtonsModule } from 'ng2-bootstrap/ng2-bootstrap';

import { FileComponent } from './file.component';
import { XMLService } from './services/xml.service';
import { StringService } from './services/string.service';
import { SeditorService } from './services/seditor.service';
import { CodeMirrorDirective } from './directives/codemirror.directive';
import { SEditorComponent } from './seditor.component';
import {Ng2Webstorage} from 'ng2-webstorage';

/**
 * Do not specify providers for modules that might be imported by a lazy loaded module.
 */

@NgModule({
    imports: [CommonModule, RouterModule, ButtonsModule, FormsModule, Ng2Webstorage],
    declarations: [SEditorComponent, CodeMirrorDirective, FileComponent],
    exports: [SEditorComponent, CommonModule, FormsModule, RouterModule, ButtonsModule],
    providers: [XMLService, StringService, SeditorService]
})
export class SEditorModule {
}
