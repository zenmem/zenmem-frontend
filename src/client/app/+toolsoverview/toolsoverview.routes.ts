﻿import { Route } from '@angular/router';
import { ToolsOverviewComponent } from './toolsoverview.component';

export const ToolsOverviewRoutes: Route[] = [
    { path: '', component: ToolsOverviewComponent } // toolsoverview
];
