﻿import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: "toolsoverview",
    templateUrl: "toolsoverview.html"
})
export class ToolsOverviewComponent { }