import { Routes } from '@angular/router';
import { ToolsOverviewRoutes } from './+toolsoverview/toolsoverview.routes';
import { SEditorRoutes } from './+seditor/seditor.routes';
import { StartpageRoutes } from './+startpage/startpage.routes';
import { MEditorRoutes } from './+meditor/meditor.routes';

export const routes: Routes = [
    ...ToolsOverviewRoutes,
    ...SEditorRoutes,
    ...StartpageRoutes,
    ...MEditorRoutes
];
