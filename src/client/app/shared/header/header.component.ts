﻿import { Component} from '@angular/core';


@Component({
    moduleId: module.id,
    selector: "header",
    templateUrl: "header.html"
})
export class HeaderComponent {
    navigationCollapsed: boolean;
    constructor() {
        this.navigationCollapsed = false;
    }
}