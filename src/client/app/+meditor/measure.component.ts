﻿import { Component, Input, Output, EventEmitter, ElementRef, OnInit, OnChanges, SimpleChanges, AfterViewInit  } from '@angular/core';
import { Measure } from './types/measure';
import { createRectFromCoords, outlineRectangle, proveRectangle} from './types/helpers';
//import { Movement } from './types/movement';
declare var d3: any;
import * as moment from 'moment';

@Component({
    selector: 'mb-measures',
    template: `<ng-content></ng-content>`
})
export class MeasureComponent implements OnChanges {

    @Input() measures: Array<Measure>;
    @Input() mode: string;
    @Input() width: number;
    @Input() height: number;
    @Input() imgSrc: string;
    @Input() currentMovement: number;
    @Input() movement: number;

    private host: any;
    private svg: any;
    private facsimile: any;
    private g: any;
    private containerWidth: any;
    private aspectRatio: any;
    private scaleFactor: any;
    private relFactor: any;
    public selectedMeasures: Array<Measure> = [];
    public help: boolean = false;
    public propertiesView: boolean = true;
    private htmlElement: HTMLElement;
    private zoomBehavior: any;
    private initialized = false;
    @Output() selectionChanged: EventEmitter<Measure[]> = new EventEmitter<Measure[]>();

    ngOnChanges(changes: SimpleChanges) {
        if (!this.measures) return;
        if (!this.initialized) {
            this.redraw();
            this.initialized = true;
        }
  
        if (changes['mode'] !== undefined) {
            switch (changes['mode'].previousValue) {
                case 'drag':
                    this.deactivateDrag();
                    break;
                case 'rect':
                    this.deactivateRectDrawing();
                    break;
                case 'touch':
                    this.deactivateTouchMode();
                    break;
                case 'edit':
                    this.deactivateModification();
                    break;
                case 'resize':
                    this.deactivateModification();
                    break;
                case 'zoom':
                    this.deactivateZoom();
                    break;
                case 'scissors':
                    this.deactivateScissors();
                    break;
                case 'hscissors':
                    this.deactivateScissors();
                    break;
            }


            switch (changes['mode'].currentValue) {
                case 'drag':
                    this.activateDrag();
                    break;
                case 'rect':
                    this.activateRectDrawing();
                    break;
                case 'touch':
                    this.activateTouchMode();
                    break;
                case 'edit':
                    this.activateModification('edit');
                    break;
                case 'resize':
                    this.activateModification('resize');
                    break;
                case 'zoom':
                    this.activateZoom();
                    break;
                case 'scissors':
                    this.activateScissors('scissors');
                    break;
                case 'hscissors':
                    this.activateScissors('hscissors');
                    break;
            }
        }

        if (changes['measures'] !== undefined) {
            this.redraw();
        }
    }

    public selectMeasures(measures: Measure[]) {
        var that = this;
        this.selectedMeasures = measures;
        this.g.selectAll('circle').remove();
        this.g.selectAll('.active').classed('active', false);
        this.svg.selectAll('g').filter(function (d: Measure) {
            return that.selectedMeasures.includes(d);
        }).select('#shape').classed('active', true);
        this.selectionChanged.emit(this.selectedMeasures);
        this.restoreBehaviors();
    }

    constructor(private element: ElementRef) {
        this.htmlElement = this.element.nativeElement;
        this.host = d3.select(this.element.nativeElement);
        var that = this;
        this.zoomBehavior = d3.zoom()
            .scaleExtent([0.5, 5])
            .on('zoom', zoomed);
        function zoomed() {
            d3.select(this).attr('transform', d3.event.transform);
        }

        // https://github.com/wbkd/d3-extended
        d3.selection.prototype.moveToFront = function () {
            return this.each(function () {
                this.parentNode.appendChild(this);
            });
        };
        d3.selection.prototype.moveToBack = function () {
            return this.each(function () {
                var firstChild = this.parentNode.firstChild;
                if (firstChild) {
                    this.parentNode.insertBefore(this, firstChild);
                }
            });
        };

    }



    private buildSVG() {
       
        this.containerWidth = d3.select('#measuresDiv').node().getBoundingClientRect().width;
        var containerHeight = d3.select('#measuresDiv').node().getBoundingClientRect().height;
        this.relFactor = (this.containerWidth - 10) / this.width;
        this.aspectRatio = this.width / this.height;
        this.host.html('');
        this.svg = this.host.append('svg')
            .attr('viewBox', '0 0 ' + (this.containerWidth - 10) + ' ' + ((this.containerWidth - 10) / this.aspectRatio))
            .attr('preserveAspectRatio', 'xMidYMid')           
            .append('g');

    }
    private proveShapes() {
        this.measures.forEach(m => m.rectangle = proveRectangle(m.vertexes));
    }

    private markShapes() {
        var that = this;
        this.g.each(function (d: Measure, i: number) { that.markShape(d3.select(this), d); });
    }
    private markShape(g: any, d: Measure) {
        var that = this;
        if (d.rectangle) {
            g.select('#shapeType')
                .attr('points', function (d: Measure) {
                    var width = 32 * that.relFactor;
                    var height = 20 * that.relFactor;
                    var relvertexes = d.vertexes.map(v => that.absoluteToRelative(v, that.relFactor));
                    var centroid = d3.polygonCentroid(relvertexes);
                    var vertexes: [number, number][] = [];
                    vertexes.push([centroid[0] - 50 * that.relFactor, centroid[1] - 45 * that.relFactor]);
                    vertexes.push([centroid[0] - 50 * that.relFactor, centroid[1] - 45 * that.relFactor + height]);
                    vertexes.push([centroid[0] - 50 * that.relFactor + width, centroid[1] - 45 * that.relFactor + height]);
                    vertexes.push([centroid[0] - 50 * that.relFactor + width, centroid[1] - 45 * that.relFactor]);
                    return vertexes.join(' ');
                })
                .attr('stroke', 'white')
                .attr('stroke-width', 2)              
                .attr('fill-opacity', 0);
        }
        else {
            g.select('#shapeType')
                .attr('points', function (d: Measure) {
                    var width = 32 * that.relFactor;
                    var height = 20 * that.relFactor;
                    var relvertexes = d.vertexes.map(v => that.absoluteToRelative(v, that.relFactor));
                    var centroid = d3.polygonCentroid(relvertexes);
                    var vertexes: [number, number][] = [];
                    vertexes.push([centroid[0] - 50 * that.relFactor + width / 2, centroid[1] - 45 * that.relFactor]);
                    vertexes.push([centroid[0] - 50 * that.relFactor, centroid[1] - 45 * that.relFactor]);
                    vertexes.push([centroid[0] - 50 * that.relFactor, centroid[1] - 45 * that.relFactor + height]);
                    vertexes.push([centroid[0] - 50 * that.relFactor + width, centroid[1] - 45 * that.relFactor + height]);
                    vertexes.push([centroid[0] - 30 * that.relFactor + width, centroid[1] - 45 * that.relFactor]);            
                    return vertexes.join(' ');
                })
                .attr('stroke', 'white')
                .attr('stroke-width', 2)
                .attr('fill-opacity', 0);
        }
    }

    private drawFacsimile() {
        var that = this;
      
        var containerWidth = d3.select('#measuresDiv').node().getBoundingClientRect().width;
        var aspectRatio = this.width / this.height;
        this.facsimile = this.svg.append('svg:image').attr('xlink:href', this.imgSrc).attr('id', 'facsimile')
            .attr('width', containerWidth - 10).attr('height', (containerWidth - 10) / aspectRatio)
            .on('mousedown', function () { if (d3.event.preventDefault) d3.event.preventDefault(); })
            .on('click', unselect);
        function unselect() {
            d3.selectAll('g').select('#shape').classed('active', false);
            d3.selectAll('g').each(function () { that.hideVertexes(d3.select(this)) });
            that.selectedMeasures = [];
            that.selectionChanged.emit(that.selectedMeasures);
        }
    }

    private activateTouchMode() {
        var that = this;
        this.svg.on('click', drawRect);
        var final: boolean = true;
        var point1: number[] = [];
        var point2: number[] = [];


        function drawRect() {
            if (!final) {
                final = true;
                point2 = that.relativeToAbsolute(d3.mouse(this), that.relFactor);

                if (point1 !== []) {
                    var ordinal = 1;
                    var movement = 1;
                    if (that.measures.length != 0) {
                        ordinal = Math.max.apply(Math, that.measures.map(function (o) { return o.ordinal; })) + 1;
                        movement = Math.max.apply(Math, that.measures.map(function (o) { return o.movement; }));
                    }
                    var newMeasure = new Measure(ordinal, createRectFromCoords(point1, point2), movement);
                    newMeasure.bindToMovement(movement);
                    newMeasure.predictMeasureNumber();
                    that.measures.push(newMeasure);
                    that.selectedMeasures = [];
                    that.selectedMeasures.push(newMeasure);
                    that.selectionChanged.emit(that.selectedMeasures);                  
                    that.redraw();
                }

            }
            else {
                final = false;
                point1 = that.relativeToAbsolute(d3.mouse(this), that.relFactor);
                that.drawCircle(point1[0], point1[1]);
            }
        }
    }

    private drawCircle(x: number, y: number) {
        this.svg.append("circle")
            .attr("cx", x * this.relFactor)
            .attr("cy", y * this.relFactor)
            .attr("r", '6')
            .style("fill", "#0275d8");

    }

    private deactivateTouchMode() {
        var that = this;
        this.facsimile.on('click', null);
        that.redraw();
        //this.svg.select('#facsimile').on('click', unselect);

        /* function unselect() {
             d3.selectAll('g').select('#shape').classed('active', false);
             d3.selectAll('g').each(function () { that.hideVertexes(d3.select(this)) });
             that.selectedMeasures = [];
             that.selectionChanged.emit(that.selectedMeasures);
         }*/
    }

    private activateZoom() {
        this.svg.call(this.zoomBehavior);

    }

    private deactivateZoom() {
        this.svg.on(".zoom", null);
    }

    private activateDrag() {
        var that = this;
        this.g.on("click", click);
        this.g.filter(function (d: Measure) { return !d.locked })
            .call(d3.drag().on('start', start).on('drag', dragged).on('end', end));
        that.selectedMeasures = [];
        function start(d: Measure) {           
            if (!that.selectedMeasures.includes(d)) {
                d3.select(this).select('#shape')
                    .classed("active", true);
            }
        }

        function end(d: Measure) {
            if (!that.selectedMeasures.includes(d)) {
                d3.select(this).select('#shape')
                    .classed("active", false);
            }
        }



        function dragged(d: Measure) {
           
          
            if (!that.selectedMeasures.includes(d)) {
                d.vertexes.forEach(p => p[0] += d3.event.dx / that.relFactor);
                d.vertexes.forEach(p => p[1] += d3.event.dy / that.relFactor);
               
            }
            for (var i = 0; i < that.selectedMeasures.length; i++) {
                if (that.selectedMeasures[i].locked) {
                    break;
                }
                that.selectedMeasures[i].vertexes.forEach(p => p[0] += d3.event.dx / that.relFactor);
                that.selectedMeasures[i].vertexes.forEach(p => p[1] += d3.event.dy / that.relFactor);              
            }

            if (!that.selectedMeasures.includes(d)) { that.redrawMeasure(d3.select(this), d); }
            that.g.filter(function (m: Measure) { return that.selectedMeasures.includes(m) })
                .each(function (m: Measure) { that.redrawMeasure(d3.select(this), m) });

        }

        function click(d: Measure) {
            if (d3.event.shiftKey) {
                if (!d3.select(this).select('#shape').classed("active")) {
                    d3.select(this).select('#shape')
                        .classed("active", true);
                    that.selectedMeasures.push(d);
                    that.selectionChanged.emit(that.selectedMeasures);
                }
                else {
                    d3.select(this).select('#shape')
                        .classed("active", false);
                    var index = that.selectedMeasures.indexOf(d);
                    if (index != -1) {
                        that.selectedMeasures.splice(index, 1);
                        that.selectionChanged.emit(that.selectedMeasures);
                    }
                }
            }
            else {
                d3.selectAll('g').select('#shape').classed('active', false);
                that.selectedMeasures = [];
                d3.select(this).select('#shape')
                    .classed("active", true);
                that.selectedMeasures.push(d);
                that.selectionChanged.emit(that.selectedMeasures);
            }
        }
    }

    private deactivateDrag() {
        this.g.on('.drag', null);
        this.g.on("click", null);
        this.facsimile.on('click', null);
    }

    private activateModification(mode: string) {
        var that = this;
        this.g.on("click", click);
        this.g.filter(function (d: Measure) { return that.selectedMeasures.includes(d) && !d.locked; })
            .each(function (d: Measure) { that.drawVertexes(d3.select(this), d, 7, mode) });

        function click(d: Measure) {
            if (d.locked) {
                return;
            }
            if (d3.event.shiftKey) {
                if (!d3.select(this).select('#shape').classed("active")) {
                    d3.select(this).select('#shape')
                        .classed("active", true);
                    that.selectedMeasures.push(d);
                    that.selectionChanged.emit(that.selectedMeasures);
                    that.drawVertexes(d3.select(this), d, 7, mode);
                    d3.select(this).moveToFront();
                }
                else {
                    d3.select(this).select('#shape')
                        .classed("active", false);
                    that.hideVertexes(d3.select(this));
                    var index = that.selectedMeasures.indexOf(d);
                    if (index != -1) {
                        that.selectedMeasures.splice(index, 1);
                        that.selectionChanged.emit(that.selectedMeasures);
                    }
                }
            }
            else {
                d3.selectAll('g').select('#shape').classed('active', false);
                d3.selectAll('g').each(function () { that.hideVertexes(d3.select(this)) });        
                d3.select(this).select('#shape')
                    .classed("active", true);
                that.selectedMeasures.push(d);
                that.selectionChanged.emit(that.selectedMeasures);
                that.drawVertexes(d3.select(this), d, 7, mode);
                d3.select(this).moveToFront();
            }
        }
    }

    private deactivateModification() {
        var that = this;
        this.g.each(function (d: Measure) { that.hideVertexes(d3.select(this)); d.midpoints = []; });
        this.g.on("click", null);
        this.facsimile.on('click', null);
    }

    public removeMeasures() {
        for (var i = 0; i < this.selectedMeasures.length; i++) {
            var index = this.measures.indexOf(this.selectedMeasures[i]);
            if (index != -1) {
                this.measures[index].unbindFromMovement();
                this.measures.splice(index, 1);
                this.selectionChanged.emit(this.selectedMeasures);
            }
        }
        this.redraw();
    }

    public lock() {
        for (var i = 0; i < this.selectedMeasures.length; i++) {
            var index = this.measures.indexOf(this.selectedMeasures[i]);
            if (index != -1) {
                this.measures[index].locked = true;
            }
        }
        if (this.selectedMeasures.length > 0) {
            this.redraw();
        }
    }

    public unlock() {
        for (var i = 0; i < this.selectedMeasures.length; i++) {
            var index = this.measures.indexOf(this.selectedMeasures[i]);
            if (index != -1) {
                this.measures[index].locked = false;
            }
        }
        if (this.selectedMeasures.length > 0) {
            this.redraw();
        }
    }

    public moveToBack() {
        var that = this;
        var selected = this.g.filter(function (d: Measure) {
            return that.selectedMeasures.includes(d);
        });
        selected.each(function () { d3.select(this).moveToBack(); });
        this.svg.select('#facsimile').moveToBack();
    }

    public convertToRectangle(d?: Measure) {
        if (d != null)
        {
            var index = this.measures.indexOf(d);
            if (index != -1) {
                this.measures[index].vertexes = outlineRectangle(this.measures[index].vertexes);
            }
        }
        else {
            for (var i = 0; i < this.selectedMeasures.length; i++) {
                var index = this.measures.indexOf(this.selectedMeasures[i]);
                if (index != -1) {
                    this.measures[index].vertexes = outlineRectangle(this.measures[index].vertexes);
                }
            }
        }

        if (this.selectedMeasures.length > 0) {
            this.selectedMeasures.forEach(sm => sm.rectangle = true);
            this.redraw();
        }
    }

    public redraw() {
        this.buildSVG();
        this.drawFacsimile();
        this.drawMeasures();
        this.markShapes();
        this.restoreBehaviors();
        this.restoreSelection();
    }

    private restoreSelection() {
        var that = this;
        this.g.filter(function (d: Measure) { return that.selectedMeasures.includes(d); }).select('#shape').classed('active', true);
    }

    private activateScissors(mode: string) {
        var that = this;
        this.g.append('circle').classed('collision', true).attr('r', 6).style('fill', '#ff0000')
            .style('visibility', 'hidden');
        this.g.append('circle').classed('collision', true).attr('r', 6).style('fill', '#ff0000')
            .style('visibility', 'hidden');

        this.g.select('#shape').on('mouseover', drawpointer).on('mouseleave', removepointer).on('mousemove', movepointer).on('click', cut);


        function drawpointer(d: Measure) {
            if (that.selectedMeasures.length !== 1 || !that.selectedMeasures.includes(d)) {
                that.selectMeasures([d]);
            }
            //d3.select(this).select('#shape')
            //    .classed("mouseover", true);
            d3.select(this.parentNode).selectAll('.collision')
                .style('visibility', 'visible');
            /*d3.select(this).selectAll('.cutting-line')
                .style('visibility', 'visible');*/
        }

        function movepointer(d: Measure) {
            var mcoords = that.relativeToAbsolute(d3.mouse(this), that.relFactor);
            if (!d.rectangle) {
                return;
            }
            var ccoords: [number, number][] = [];
            if (mode == 'scissors') {
            
                ccoords = findCollisionPoints(d, mcoords[0]);
            }

            else {
               
                ccoords = findCollisionPoints(d, mcoords[1]);
            }

            var colDiff = ccoords.length - d3.select(this.parentNode).selectAll('.collision').size();

            //ccoords.map(p => that.absoluteToRelative(p, that.relFactor));

            if (colDiff > 0) {
                for (var i = 0; i < colDiff; i++) {
                    d3.select(this.parentNode).append('circle').classed('collision', true).attr('r', 6).style('fill', '#ff0000');
                }
            }
            else if (colDiff < 0) {
                for (var i = 0; i < colDiff * -1; i++) {
                    d3.select(this.parentNode).select('.collision').remove();
                }
            }

            d3.select(this.parentNode).selectAll('.collision').each(function (d: Measure, i: number) {
                if (!isNaN(ccoords[i][1])) {
                    d3.select(this).attr('cx', function (d: Measure) { return ccoords[i][0] * that.relFactor; })
                        .attr('cy', function (d: Measure) { return ccoords[i][1] * that.relFactor; });
                }
            });

        }

        function cut(d: Measure) {
            if (!d.rectangle) {
                alert("Please convert the polygon to rectangle and try again!");
                return;
            }
            var mcoords = that.relativeToAbsolute(d3.mouse(this), that.relFactor);
           
            if (mode == 'scissors') {
                var ccoords = findCollisionPoints(d, mcoords[0]);            
            }

            else {
                var ccoords = findCollisionPoints(d, mcoords[1]);              
            }

            var index = that.measures.indexOf(d);
           // ccoords.forEach(p => p[0] *= that.relFactor);
            //ccoords.forEach(p => p[1] *= that.relFactor);
            //d.vertexes.forEach(p => p[0] *= that.relFactor);
            //d.vertexes.forEach(p => p[1] *= that.relFactor);
            
            if (index > -1) {
                var newMe1: Measure = new Measure(d.ordinal);
                newMe1.movement = d.movement;
                newMe1.suffix = d.suffix;
                newMe1.excluded = d.excluded;
                newMe1.group = d.group;
                newMe1.locked = d.locked;
                newMe1.parent = d.parent;
                newMe1.rectangle = true;
                newMe1.upbeat = d.upbeat;
                newMe1.visible = d.visible;
                newMe1.pause = d.pause;

               
                if (mode == 'scissors') {

                    newMe1.vertexes.push(d.vertexes[0]);
                    newMe1.vertexes.push(d.vertexes[1]);
                    newMe1.vertexes.push(ccoords[0]);
                    newMe1.vertexes.push(ccoords[1]);
                }
                            
                else {          

                    newMe1.vertexes.push(ccoords[0]);
                    newMe1.vertexes.push(d.vertexes[1]);
                    newMe1.vertexes.push(d.vertexes[2]);
                    newMe1.vertexes.push(ccoords[1]);
                }

                newMe1.bindToMovement(d.movement);

                var newMe2: Measure = new Measure(Math.max.apply(Math, that.measures.map(function (o) { return o.ordinal; })) + 1);
                newMe2.movement = d.movement;
                newMe2.suffix = d.suffix;
                newMe2.excluded = d.excluded;
                newMe2.group = d.group;
                newMe2.locked = d.locked;
                newMe2.parent = d.parent;
                newMe2.rectangle = true;
                newMe2.upbeat = d.upbeat;
                newMe2.visible = d.visible;
                newMe2.pause = d.pause;

                if (mode == 'scissors') {

                    newMe2.vertexes.push(ccoords[1]);
                    newMe2.vertexes.push(ccoords[0]);
                    newMe2.vertexes.push(d.vertexes[2]);
                    newMe2.vertexes.push(d.vertexes[3]);
                }

                else {    
                         
                    newMe2.vertexes.push(ccoords[0]); 
                    newMe2.vertexes.push(d.vertexes[0]);
                    newMe2.vertexes.push(d.vertexes[3]); 
                    newMe2.vertexes.push(ccoords[1]);                
                }
                newMe2.bindToMovement(d.movement);
                
                that.measures.splice(index, 1, newMe1);
                that.measures.splice(index + 1, 0, newMe2);
                that.convertToRectangle(newMe1);
                that.convertToRectangle(newMe2);
               
               // that.measures.splice(index + 1, 0, newMe2);
            }



            /*var cuttedIndexes: number[] = [];
            cuttedVertexes.forEach(cv => cuttedIndexes.push(d.vertexes.indexOf(cv)));
            var minIndex = Math.min.apply(null, cuttedIndexes);
            var maxIndex = Math.max.apply(null, cuttedIndexes);
            var savedVertexes: [number, number][] = [];
            var newPolygons: [number, number][][] = [];
            var newPoly: [number, number][] = [];
            var j = 1;
            newPoly.push([ccoords[0][0], ccoords[0][1]]);
            for (var i = minIndex; i <= maxIndex; i++) {
                if (!cuttedVertexes.includes(d.vertexes[i])) {
                    savedVertexes.push(d.vertexes[i]);
                    newPoly.push([ccoords[j][0], ccoords[j][1]]);
                    newPolygons.push(newPoly.slice());
                    j++;

                    newPoly = [];
                    newPoly.push([ccoords[j][0], ccoords[j][1]]);
                    j++;
                }
                else {
                    newPoly.push([d.vertexes[i][0], d.vertexes[i][1]]);
                }
            }
            newPoly.push([ccoords[j][0], ccoords[j][1]]);
            newPolygons.push(newPoly.slice());

           
           var frontier: [number, number][] = ccoords.concat(savedVertexes)
                .sort(function (a: [number, number], b: [number, number]) {
                    return b[1] - a[1];
                });
            

            var args = [minIndex, maxIndex - minIndex + 1].concat(frontier);
            Array.prototype.splice.apply(d.vertexes, args);

            var index = that.measures.indexOf(d);
            for (var x = index + 1; x < that.measures.length; x++) {
                that.measures[x].ordinal += newPolygons.length;
            }
            var ordinal = d.ordinal;
            for (var k = newPolygons.length - 1; k >= 0; k--) {
                that.measures.splice(++index, 0,
                    new Measure(++ordinal, newPolygons[k].slice(), d.suffix, d.parent, d.group, null, d.visible, d.excluded, d.upbeat, d.locked, d.pause, d.getMovement()));
            }*/
            that.redraw();

          
        }


        function findCollisionPoints(d: Measure, point: number): [number, number][] {
            var result: [number, number][] = [];
           // d.vertexes.map(v => that.absoluteToRelative(v, that.relFactor));
            for (var i = 1; i < d.vertexes.length; i++) {
                var newPoint = detectCollisionPoint(d.vertexes[i - 1], d.vertexes[i], point, mode);
                if (newPoint !== null) {                   
                    result.push(newPoint);
                }
            }

            var newPoint = detectCollisionPoint(d.vertexes[d.vertexes.length - 1], d.vertexes[0], point, mode);
            if (newPoint !== null && !isNaN(newPoint[1])) {
                result.push(newPoint);
            }
           
            return result;
        }

        function detectCollisionPoint(p1: number[], p2: number[], point: number, mode: string): [number, number] {
          
            if (mode == 'scissors') {
                if (point < Math.min(p1[0], p2[0]) || point > Math.max(p1[0], p2[0])) {
                    return null;
                }

                var a = (p2[1] - p1[1]) / (p2[0] - p1[0]);
                var b = p1[1] - p1[0] * a;
                return [point, (a * point + b)];
            }

            else {
                if (point < Math.min(p1[1], p2[1]) || point > Math.max(p1[1], p2[1])) {
                    return null;
                }

                return [p1[0], point];
            }


        }


        function removepointer() {
            d3.select(this).selectAll('.collision').style('visibility', 'hidden');
            /*d3.select(this).selectAll('.cutting-line').style('visibility', 'hidden');*/
        }
    }



    private deactivateScissors() {
        this.g.selectAll('#pointer').remove();
        this.g.selectAll('.collision').remove();
        /*this.g.selectAll('.cutting-line').remove();*/
        this.g.on('mouseover', null).on('mouseleave', null).on('mousemove', null).on('click', null);
        this.redraw();
    }

    


    private activateRectDrawing() {

        this.svg.on('mousedown', drawingstarted).on('mouseup', drawingended).on('mousemove', drawpreview);
        var mouseDown: boolean = false;
        var svg = this.svg;
        var start: number[];
        var end: number[];
        var that = this;
        var preview = that.svg.select('polygon.preview');
        if (preview.empty()) {
            preview = that.svg.append('polygon')
                .classed('preview', true)
                .attr('points', function () { return [[0, 0], [0, 0], [0, 0], [0, 0]]; })
                .attr('fill', '#000000')
                .attr('fill-opacity', 0.1);
        }
        function drawingstarted() {
            mouseDown = true;
            start = that.relativeToAbsolute(d3.mouse(this), that.relFactor);
        }

        function drawpreview() {
            if (mouseDown) {
                end = that.relativeToAbsolute(d3.mouse(this), that.relFactor);
                var previewPoints = createRectFromCoords(start, end);
                preview.attr('points', function () {
                    return previewPoints.map(v => that.absoluteToRelative(v, that.relFactor)
                    ).join(' ');
                });
            }
        }

        function drawingended() {
            mouseDown = false;
            end = that.relativeToAbsolute(d3.mouse(this), that.relFactor);
            var area = Math.abs(start[0] - end[0]) * Math.abs(start[1] - end[1]);
            if (area < 25)
                return;
            var ordinal = 1;
            var movement = 1;
            if (that.measures.length != 0) {
                ordinal = Math.max.apply(Math, that.measures.map(function (o) { return o.ordinal; })) + 1;
                movement = Math.max.apply(Math, that.measures.map(function (o) { return o.movement; }));
            }
            var newMeasure = new Measure(ordinal, createRectFromCoords(start, end), movement);
            newMeasure.bindToMovement(movement);
            newMeasure.predictMeasureNumber();
            that.measures.push(newMeasure);
            that.selectedMeasures = [];
            that.selectedMeasures.push(newMeasure);
            that.selectionChanged.emit(that.selectedMeasures);
            that.redraw();
        }
    }

    private deactivateRectDrawing() {
        this.svg.on('mousedown', null).on('mouseup', null).on('mousemove', null);

    }

    private absoluteToRelative = function (point: number[], factor: number): number[] {
        return point.map(function (coord) {
            return coord * factor;
        });

    }

    private relativeToAbsolute = function (point: number[], factor: number): number[] {
        return point.map(function (coord) {
            return coord / factor;
        });
    }

    private drawMeasures() {
        var that = this;
        this.g = this.svg.selectAll('g')
            .data(this.measures, function (d: Measure) {
                return d.getMeasureName();
            })
            .enter()
            .append('g')
            .style("visibility", function (d: Measure) {
                return d.visible ? "visible" : "hidden";
            });
        this.g.append('polygon')
            .attr('points', function (d: Measure) {
                 return d.vertexes.map(v => that.absoluteToRelative(v, that.relFactor)).join(' ');
             })
            .attr('id', 'shape')
            .attr('fill', '#000000')
            .attr('fill-opacity', 0.2)
            .attr('stroke-width', 1)
            .attr('stroke', '#000000');
        
        this.g.append('polygon')
            .attr('id', 'shapeType');
        this.g.on("mouseover", function () {
            d3.select(this).select('#shape')
                .classed("mouseover", true);
        })
            .on("mouseleave", function () {
                d3.select(this).select('#shape')
                    .classed("mouseover", false);
            });
       
        this.g.filter(function (d: Measure) { return d.excluded }).append('line')
            .attr('id', 'excluded1')
            .attr('stroke', 'red')
            .attr('stroke-width', 2)
            .attr('x1', function (d: Measure) {
                var relvertexes = d.vertexes.map(v => that.absoluteToRelative(v, that.relFactor));
                var xVals = relvertexes.map(function (obj) {
                    return obj[0];
                });
                var yVals = relvertexes.map(function (obj) {
                    return obj[1];
                });
                var centroid = d3.polygonCentroid(relvertexes);
                var minX = Math.min.apply(null, xVals);
                var minY = Math.min.apply(null, yVals);
                var maxX = Math.max.apply(null, xVals);
                var maxY = Math.max.apply(null, yVals);
                return centroid[0] - (maxX - minX) / 4;
            })
            .attr('x2', function (d: Measure) {
                var relvertexes = d.vertexes.map(v => that.absoluteToRelative(v, that.relFactor));
                var xVals = relvertexes.map(function (obj) {
                    return obj[0];
                });
                var yVals = relvertexes.map(function (obj) {
                    return obj[1];
                });
                var centroid = d3.polygonCentroid(relvertexes);
                var minX = Math.min.apply(null, xVals);
                var minY = Math.min.apply(null, yVals);
                var maxX = Math.max.apply(null, xVals);
                var maxY = Math.max.apply(null, yVals);
                return centroid[0] + (maxX - minX) / 4;
            })
            .attr('y1', function (d: Measure) {
                var relvertexes = d.vertexes.map(v => that.absoluteToRelative(v, that.relFactor));
                var xVals = relvertexes.map(function (obj) {
                    return obj[0];
                });
                var yVals = relvertexes.map(function (obj) {
                    return obj[1];
                });
                var centroid = d3.polygonCentroid(relvertexes);
                var minX = Math.min.apply(null, xVals);
                var minY = Math.min.apply(null, yVals);
                var maxX = Math.max.apply(null, xVals);
                var maxY = Math.max.apply(null, yVals);
                return centroid[1] - (maxY - minY) / 4;
            })
            .attr('y2', function (d: Measure) {
                var relvertexes = d.vertexes.map(v => that.absoluteToRelative(v, that.relFactor));
                var xVals = relvertexes.map(function (obj) {
                    return obj[0];
                });
                var yVals = relvertexes.map(function (obj) {
                    return obj[1];
                });
                var centroid = d3.polygonCentroid(relvertexes);
                var minX = Math.min.apply(null, xVals);
                var minY = Math.min.apply(null, yVals);
                var maxX = Math.max.apply(null, xVals);
                var maxY = Math.max.apply(null, yVals);
                return centroid[1] + (maxY - minY) / 4;
            });
        this.g.filter(function (d: Measure) { return d.excluded }).append('line')
            .attr('id', 'excluded2')
            .attr('stroke', 'red')
            .attr('stroke-width', 2)
            .attr('x1', function (d: Measure) {
                var relvertexes = d.vertexes.map(v => that.absoluteToRelative(v, that.relFactor));
                var xVals = relvertexes.map(function (obj) {
                    return obj[0];
                });
                var yVals = relvertexes.map(function (obj) {
                    return obj[1];
                });
                var centroid = d3.polygonCentroid(relvertexes);
                var minX = Math.min.apply(null, xVals);
                var minY = Math.min.apply(null, yVals);
                var maxX = Math.max.apply(null, xVals);
                var maxY = Math.max.apply(null, yVals);
                return centroid[0] - (maxX - minX) / 4;
            })
            .attr('x2', function (d: Measure) {
                var relvertexes = d.vertexes.map(v => that.absoluteToRelative(v, that.relFactor));
                var xVals = relvertexes.map(function (obj) {
                    return obj[0];
                });
                var yVals = relvertexes.map(function (obj) {
                    return obj[1];
                });
                var centroid = d3.polygonCentroid(relvertexes);
                var minX = Math.min.apply(null, xVals);
                var minY = Math.min.apply(null, yVals);
                var maxX = Math.max.apply(null, xVals);
                var maxY = Math.max.apply(null, yVals);
                return centroid[0] + (maxX - minX) / 4;
            })
            .attr('y1', function (d: Measure) {
                var relvertexes = d.vertexes.map(v => that.absoluteToRelative(v, that.relFactor));
                var xVals = relvertexes.map(function (obj) {
                    return obj[0];
                });
                var yVals = relvertexes.map(function (obj) {
                    return obj[1];
                });
                var centroid = d3.polygonCentroid(relvertexes);
                var minX = Math.min.apply(null, xVals);
                var minY = Math.min.apply(null, yVals);
                var maxX = Math.max.apply(null, xVals);
                var maxY = Math.max.apply(null, yVals);
                return centroid[1] + (maxY - minY) / 4;
            })
            .attr('y2', function (d: Measure) {
                var relvertexes = d.vertexes.map(v => that.absoluteToRelative(v, that.relFactor));
                var xVals = relvertexes.map(function (obj) {
                    return obj[0];
                });
                var yVals = relvertexes.map(function (obj) {
                    return obj[1];
                });
                var centroid = d3.polygonCentroid(relvertexes);
                var minX = Math.min.apply(null, xVals);
                var minY = Math.min.apply(null, yVals);
                var maxX = Math.max.apply(null, xVals);
                var maxY = Math.max.apply(null, yVals);
                return centroid[1] - (maxY - minY) / 4;
            });
        this.g.append('text')
            .text(function (d: Measure) { return d.getMeasureName(); })
            .attr('id', 'name')
            .classed('disable-select', true)
            .attr('x', function (d: Measure) {
                var relvertexes = d.vertexes.map(v => that.absoluteToRelative(v, that.relFactor));
                var centroid = d3.polygonCentroid(relvertexes);
                return centroid[0];
            })
            .attr('y', function (d: Measure) {
                var relvertexes = d.vertexes.map(v => that.absoluteToRelative(v, that.relFactor));
                var centroid = d3.polygonCentroid(relvertexes);
                return centroid[1];
            })
            .attr('text-anchor', 'middle').attr('alignment-baseline', 'middle')
            .attr('fill', 'white').attr('font-size', 16);
        /*this.g.append('text')
            .attr('id', 'movement')
            .classed('disable-select', true)
            .text(function (d: Measure) { return 'm' + d.getMovementOrdinal(); })
            .attr('x', function (d: Measure) {
                var centroid = d3.polygonCentroid(d.vertexes); return centroid[0] - 30;
            })
            .attr('y', function (d: Measure) {
                var centroid = d3.polygonCentroid(d.vertexes); return centroid[1] - 30;
            }).attr('text-anchor', 'middle').attr('alignment-baseline', 'middle')
            .attr('fill', 'white').attr('font-size', 12);*/
        this.g.filter(function (d: Measure) { return d.locked }).append('text')
            .attr('id', 'lock')
            .classed('disable-select', true)
            .attr('font-family', 'FontAwesome')
            .text(function (d: Measure) { return '\uf023' })
            .attr('x', function (d: Measure) {
                var relvertexes = d.vertexes.map(v => that.absoluteToRelative(v, that.relFactor));
                var centroid = d3.polygonCentroid(relvertexes);
                return centroid[0] + 20;
            })
            .attr('y', function (d: Measure) {
                var relvertexes = d.vertexes.map(v => that.absoluteToRelative(v, that.relFactor));
                var centroid = d3.polygonCentroid(relvertexes);
                return centroid[1] - 30;
            }).attr('text-anchor', 'middle').attr('alignment-baseline', 'middle')
            .attr('fill', 'white').attr('font-size', 12);
        this.g.filter(function (d: Measure) { return d.upbeat }).append('text')
            .attr('id', 'upbeat')
            .classed('disable-select', true)
            .text(function (d: Measure) { return 'U' })
            .attr('x', function (d: Measure) {
                var relvertexes = d.vertexes.map(v => that.absoluteToRelative(v, that.relFactor));
                var centroid = d3.polygonCentroid(relvertexes);
                return centroid[0] + 20;
            })
            .attr('y', function (d: Measure) {
                var relvertexes = d.vertexes.map(v => that.absoluteToRelative(v, that.relFactor));
                var centroid = d3.polygonCentroid(relvertexes);
                return centroid[1] + 30;
            }).attr('text-anchor', 'middle').attr('alignment-baseline', 'middle')
            .attr('fill', 'white').attr('font-size', 12);
        this.g.filter(function (d: Measure) { return d.pause > 0 }).append('text')
            .attr('id', 'pause')
            .classed('disable-select', true)
            .text(function (d: Measure) { return 'P: ' + d.pause })
            .attr('x', function (d: Measure) {
                var relvertexes = d.vertexes.map(v => that.absoluteToRelative(v, that.relFactor));
                var centroid = d3.polygonCentroid(relvertexes);
                return centroid[0] - 20;
            })
            .attr('y', function (d: Measure) {
                var relvertexes = d.vertexes.map(v => that.absoluteToRelative(v, that.relFactor));
                var centroid = d3.polygonCentroid(relvertexes);
                return centroid[1] + 30;
            }).attr('text-anchor', 'middle').attr('alignment-baseline', 'middle')
            .attr('fill', 'white').attr('font-size', 12);
        this.g.exit().remove();
        this.proveShapes();
        this.markShapes();
    }
    private redrawMeasure(g: any, d: Measure) {
        d.rectangle = proveRectangle(d.vertexes);
        g.style("visibility", function (d: Measure) {
            return d.visible ? "visible" : "hidden";
        })
        var relvertexes = d.vertexes.map(v => this.absoluteToRelative(v, this.relFactor));

        g.select('#shape').attr('points', function (d: Measure) {
            return relvertexes.join(' ');
        });
        var xVals = relvertexes.map(function (obj) {
            return obj[0];
        });
        var yVals = relvertexes.map(function (obj) {
            return obj[1];
        });
        var centroid = d3.polygonCentroid(relvertexes);
        var minX = Math.min.apply(null, xVals);
        var minY = Math.min.apply(null, yVals);
        var maxX = Math.max.apply(null, xVals);
        var maxY = Math.max.apply(null, yVals);
        g.select('#excluded1')
            .attr('x1', function (d: Measure) {   
                return centroid[0] - (maxX - minX) / 4;
            })
            .attr('x2', function (d: Measure) {
                return centroid[0] + (maxX - minX) / 4;
            })
            .attr('y1', function (d: Measure) {
                return centroid[1] - (maxY - minY) / 4;
            })
            .attr('y2', function (d: Measure) {
                return centroid[1] + (maxY - minY) / 4;
            });
        g.select('#excluded2')
            .attr('x1', function (d: Measure) {
                return centroid[0] - (maxX - minX) / 4;
            })
            .attr('x2', function (d: Measure) {
                return centroid[0] + (maxX - minX) / 4;
            })
            .attr('y1', function (d: Measure) {
                return centroid[1] + (maxY - minY) / 4;
            })
            .attr('y2', function (d: Measure) {
                return centroid[1] - (maxY - minY) / 4;
            });
        g.select('#name')
            .attr('x', function (d: Measure) { return centroid[0]; })
            .attr('y', function (d: Measure) { return centroid[1]; });
        /*g.select('#movement')
            .attr('x', function (d: Measure) {
                var centroid = d3.polygonCentroid(d.vertexes); return centroid[0] - 30;
            })
            .attr('y', function (d: Measure) {
                var centroid = d3.polygonCentroid(d.vertexes); return centroid[1] - 30;
            });*/
        g.select('#lock')
            .attr('x', function (d: Measure) {
                return centroid[0] + 20;
            })
            .attr('y', function (d: Measure) {
                return centroid[1] - 30;
            });
        g.select('#upbeat')
            .attr('x', function (d: Measure) {
                return centroid[0] + 20;
            })
            .attr('y', function (d: Measure) {
                return centroid[1] + 30;
            });
        g.select('#pause')
            .attr('x', function (d: Measure) {
                return centroid[0] - 20;
            })
            .attr('y', function (d: Measure) {
                return centroid[1] + 30;
            });
        this.markShape(g, d);
    }

    private drawVertexes(g: any, d: Measure, r: number, mode: string) {
        var that = this;
        var vertexDragged: any;
        var oldPosition: number[] = [];
        if (mode === 'edit') vertexDragged = editVertexDragged;
        else if (mode === 'resize') vertexDragged = resizeVertexDragged;
        var midpointsDragended: any;
        if (mode === 'edit') midpointsDragended = editMidpointsDragended;
        else if (mode === 'resize') midpointsDragended = resizeMidpointsDragended;
        var midpointsDragged: any;
        if (mode === 'edit') midpointsDragged = editMidpointsDragged;
        else if (mode === 'resize') midpointsDragged = resizeMidpointsDragged;

        if (mode === 'edit') { drawVertexes(); }
        calculateMidpoints();
        drawMidpoints();

        function drawVertexes() {
            var relvertex = d.vertexes.map(v => that.absoluteToRelative(v, that.relFactor));
            for (var i = 0; i < relvertex.length; i++) {
                g.append('circle')
                    .call(d3.drag().on('start', vertexDragstarted)
                        .on('drag', vertexDragged)
                        .on('end', vertexDragended))
                    .on('dblclick', removeVertex)
                    .attr('id', 'vx' + i)
                    .attr('cx', function (d: Measure) { return relvertex[i][0]; })
                    .attr('cy', function (d: Measure) { return relvertex[i][1]; })
                    .attr("r", r)
                    .style("fill", "#0275d8");
            }
        }

        function removeVertex(d: Measure) {
            if (d.vertexes.length <= 3) return;
            var index = parseInt(this.id.match(/\d+$/)[0]);
            for (var i = index + 1; i < d.vertexes.length; i++) {
                g.select('#vx' + i).attr('id', 'vx' + (i - 1));
            }
            d.vertexes.splice(index, 1);
            d3.select(this).remove();
            that.redraw();
        }

        function drawMidpoints() {
            var relmidpoints = d.midpoints.map(v => that.absoluteToRelative(v, that.relFactor));

            for (var i = 0; i < relmidpoints.length; i++) {             
                g.append('circle')
                    .call(d3.drag().on('start', midpointDragstarted)
                        .on('drag', midpointsDragged)
                        .on('end', midpointsDragended))
                    .attr('id', 'mp' + i)
                    .attr('cx', function (d: Measure) { return relmidpoints[i][0]; })
                    .attr('cy', function (d: Measure) { return relmidpoints[i][1]; })
                    .attr("r", r)
                    .style("fill", "#ff0000");
            }
        }

        
        function calculateMidpoints() {
            d.midpoints = [];
            // d.vertexes.map(v => that.absoluteToRelative(v, that.relFactor));
            d.midpoints.push([((d.vertexes[d.vertexes.length - 1][0]  + d.vertexes[0][0]) / 2),
                ((d.vertexes[d.vertexes.length - 1][1] + d.vertexes[0][1]) / 2)]);
            for (var i = 1; i < d.vertexes.length; i++) {
                d.midpoints.push([((d.vertexes[i - 1][0] + d.vertexes[i][0]) / 2),
                    ((d.vertexes[i - 1][1] + d.vertexes[i][1]) / 2)]);
            }
            d.midpoints.map(v => that.absoluteToRelative(v, that.relFactor));


        }

        function vertexDragstarted(d: Measure) {
            var index = parseInt(this.id.match(/\d+$/)[0]);
            d3.select(this).raise().classed('active', true);
        }

        function midpointDragstarted(d: Measure) {
            var index = parseInt(this.id.match(/\d+$/)[0]);
            d3.select(this).raise().classed('active', true);
            oldPosition = d.midpoints[index].slice();
           
            g.append('line')
                .attr('id', 'midline')
                .attr('stroke', 'red')
                .attr('stroke-width', 1)
                .style('stroke-dasharray', ('3,3'))
                .attr('x1', oldPosition[0] * that.relFactor )
                .attr('x2', oldPosition[0] * that.relFactor)
                .attr('y1', oldPosition[1] * that.relFactor)
                .attr('y2', oldPosition[1] * that.relFactor);
        }

        function editMidpointsDragended(d: Measure) {
            var index = parseInt(this.id.match(/\d+$/)[0]);
            d3.select(this).classed('active', false);
            var coordinates = that.relativeToAbsolute(d3.mouse(this), that.relFactor);
            var position: any = [coordinates[0], coordinates[1]];

            d.vertexes.splice(index, 0, position);

            that.redrawMeasure(g, d);

            g.selectAll('circle').remove();
            drawVertexes();
            calculateMidpoints();
            drawMidpoints();

            g.select('#midline').remove();
        }

        function resizeVertexDragged(d: Measure) {
            var index = parseInt(this.id.match(/\d+$/)[0]);
        }

        function resizeMidpointsDragended(d: Measure) {
            var index = parseInt(this.id.match(/\d+$/)[0]);
            d3.select(this).classed('active', false);
            var newPosition = that.relativeToAbsolute(d3.mouse(this), that.relFactor);
            var dx = (newPosition[0] - oldPosition[0]);
            var dy = (newPosition[1] - oldPosition[1]);


            if (index === 0) {
                d.vertexes[0][0] += dx;
                d.vertexes[0][1] += dy;
                d.vertexes[d.vertexes.length - 1][0] += dx;
                d.vertexes[d.vertexes.length - 1][1] += dy;
            }
            else {
                d.vertexes[index][0] += dx;
                d.vertexes[index][1] += dy;
                d.vertexes[index - 1][0] += dx;
                d.vertexes[index - 1][1] += dy;
            }

            that.redrawMeasure(g, d);

            g.selectAll('circle').remove();
            calculateMidpoints();
            drawMidpoints();

            g.select('#midline').remove();
        }

        function editMidpointsDragged(d: Measure) {
            var index = parseInt(this.id.match(/\d+$/)[0]);

            d.midpoints[index][0] += d3.event.dx / that.relFactor;
            d.midpoints[index][1] += d3.event.dy / that.relFactor;

            d3.select(this).attr('cx', function (d: Measure) { return d.midpoints[index][0] * that.relFactor; })
                .attr('cy', function (d: Measure) { return d.midpoints[index][1] * that.relFactor; });

            g.select('#midline')
                .attr('x1', oldPosition[0] * that.relFactor)
                .attr('x2', oldPosition[0] * that.relFactor)
                .attr('x2', d.midpoints[index][0] * that.relFactor)
                .attr('y2', d.midpoints[index][1] * that.relFactor);
        }

        function resizeMidpointsDragged(d: Measure) {
            var index = parseInt(this.id.match(/\d+$/)[0]);
            d.midpoints[index][0] += d3.event.dx / that.relFactor;
            d.midpoints[index][1] += d3.event.dy / that.relFactor;

            d3.select(this).attr('cx', function (d: Measure) { return d.midpoints[index][0] * that.relFactor; })
                .attr('cy', function (d: Measure) { return d.midpoints[index][1] * that.relFactor; });

            g.select('#midline')
                .attr('x2', d.midpoints[index][0] * that.relFactor)
                .attr('y2', d.midpoints[index][1] * that.relFactor );
        }


        function editVertexDragged(d: Measure) {
            var index = parseInt(this.id.match(/\d+$/)[0]);

            d.vertexes[index][0] += d3.event.dx / that.relFactor;
            d.vertexes[index][1] += d3.event.dy / that.relFactor;

            d3.select(this).attr('cx', function (d: Measure) { return d.vertexes[index][0] * that.relFactor; })
                .attr('cy', function (d: Measure) { return d.vertexes[index][1] * that.relFactor; });

            that.redrawMeasure(g, d);

            calculateMidpoints();

            for (var i = 0; i < d.midpoints.length; i++) {
                g.select('#mp' + i)
                    .attr('cx', function (d: Measure) { return d.midpoints[i][0] * that.relFactor; })
                    .attr('cy', function (d: Measure) { return d.midpoints[i][1] * that.relFactor; });
            }
        }

        function vertexDragended(d: Measure) {
            d3.select(this).classed('active', false);
        }

    }

    private hideVertexes(g: any) {
        g.selectAll('circle').remove();
    }

    private restoreBehaviors() {
        switch (this.mode) {
            case 'drag':
                this.activateDrag();
                break;
            case 'zoom':
                this.activateZoom();
                break;
            case 'rect':
                this.activateRectDrawing();
                break;
            case 'touch':
                this.activateTouchMode();
                break;
            case 'edit':
                this.activateModification('edit');
                break;
            case 'resize':
                this.activateModification('resize');
                break;
            case 'scissors':
                this.activateScissors('scissors');
                break;
            case 'hscissors':
                this.activateScissors('hscissors');
                break;
        }
    }

   

}