﻿import { Pipe, PipeTransform, ChangeDetectionStrategy } from '@angular/core';
import { Measure } from '../types/measure';

@Pipe({
    name: 'movementOrdinal'
})
export class MovementOrdinalPipe implements PipeTransform {
    transform(value: Measure): number {
        return value.getMovementOrdinal();
    }
}