﻿import { Pipe, PipeTransform, ChangeDetectionStrategy } from '@angular/core';
import { Measure } from '../types/measure';
import { MeasureGroup } from '../types/measureGroup';

@Pipe({
    name: 'measureName',
    pure: false
})
export class MeasureNamePipe implements PipeTransform {
    transform(value: Measure): string {
        return value.getMeasureName();
    }
}