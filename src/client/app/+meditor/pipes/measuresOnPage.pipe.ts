﻿import {Pipe, PipeTransform} from '@angular/core';
import { Measure } from '../types/measure';

// Tell Angular2 we're creating a Pipe with TypeScript decorators
@Pipe({
    name: 'measuresOnPage',
    pure: false
})
export class MeasuresOnPagePipe implements PipeTransform {
    transform(measures: Measure[], pageMeasures: Measure[]): Measure[] {
        return measures.filter(me => pageMeasures.includes(me)).sort(Measure.compareByOrdinals);
    }
}