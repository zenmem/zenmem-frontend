﻿import { Pipe, PipeTransform } from '@angular/core';
import { ItemPage } from '../types/itemPage';

@Pipe({ name: 'paginator' })
export class PaginatorPipe implements PipeTransform {
    transform(value: ItemPage[], index: string): ItemPage[] {
        var i = parseInt(index);
        if (value.length < 3) {
            return value;
        }
        if (i === 0) {
            return [value[0], value[1], value[2]];
        }
        if (i === value.length - 1) {
            return [value[value.length - 3], value[value.length - 2], value[value.length - 1]];
        }
        else {
            return [value[i - 1], value[i], value[i + 1]];
        }
    }
}