﻿import { Pipe, PipeTransform, ChangeDetectionStrategy } from '@angular/core';

@Pipe({
    name: 'modeCreationIcon'
})

export class ModeCreationIconPipe implements PipeTransform {
    transform(mode: string): string {
        switch (mode) {
            case 'scissors':
                return 'fa fa-scissors fa-lg fa-rotate-90';
            case 'points':
                return 'fa fa-share-alt fa-lg';
            case 'touch':
                return 'fa fa-dot-circle-o fa-lg'
            case 'hscissors':
                return 'fa fa-scissors fa-lg';
            default:
                return 'fa fa-square-o fa-lg';
        }
    }
}