﻿import { Pipe, PipeTransform, ChangeDetectionStrategy } from '@angular/core';

@Pipe({
    name: 'modeModificationIcon'
})

export class ModeModificationIconPipe implements PipeTransform {
    transform(mode: string): string {
        switch (mode) {
            case 'edit':
                return 'fa fa-pencil fa-lg';
            case 'resize':
                return 'fa fa-arrows-alt fa-lg';
            default:
                return 'fa fa-pencil fa-lg';
        }
    }
}