﻿import { Measure } from './measure';

export class MeasureGroup {
    measures: Measure[];
    ordinal: number;

    constructor(ordinal: number, measures?: Measure[]) {
        this.ordinal = ordinal;
        this.measures = measures || [];
    }
}