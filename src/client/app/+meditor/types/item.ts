﻿import { ItemPage } from './itemPage';
//import { Movement } from './movement';

export class Item {
    workTitle: string;
    expressionTitle: string;
    manifestationTitle: string;
    id: string;
    status: string;
    pages: ItemPage[];
    //movements: Movement[];
    //currentMovement: number;
    constructor(id: string, workTitle?: string, expressionTitle?: string,
        manifestationTitle?: string, status?: string, pages?: ItemPage[]){//, movements?: Movement[], currentMovement?: number) {
        this.id = id;
        this.workTitle = workTitle || "unknown";
        this.expressionTitle = expressionTitle || "unknown";
        this.manifestationTitle = manifestationTitle || "unknown";
        this.status = status || "unknown";
        this.pages = pages || [];
      //  this.movements = movements || [];
      //  this.currentMovement = currentMovement || 0;
    }
}