﻿import { MeasureGroup } from './measureGroup';
//import { Movement } from './movement';

declare var _: any;

export class Measure {
    ordinal: number;
    vertexes: [number, number][];
    midpoints: [number, number][];
    movement: number;
    parent: Measure;
    group: MeasureGroup;
    suffix: string;
    visible: boolean;
    pause: number;
    upbeat: boolean;
    excluded: boolean;
    locked: boolean;
    rectangle: boolean;

    constructor(ordinal: number, vertexes?: [number, number][], movement?: number, suffix?: string,
        parent?: Measure, group?: MeasureGroup, midpoints?: [number, number][],
        visible?: boolean, excluded?: boolean, upbeat?: boolean, locked?: boolean,
        pause?: number, rectangle?: boolean) {
        this.ordinal = ordinal;
        this.vertexes = vertexes || [];
        this.midpoints = midpoints || [];
        this.suffix = suffix || "";
        this.parent = parent || null;
        this.group = group || null;
        this.visible = visible || true;
        this.excluded = excluded || false;
        this.upbeat = upbeat || false;
        this.locked = locked || false;
        this.movement = movement || 0;
        this.pause = pause || 0;
        this.rectangle = rectangle || false;
        /*if (movement) {
            this.bindToMovement(movement);
        }
        else {
            this.movement = null;
        }*/
    }

    public bindToMovement(movement: number) {
     /*   if (this.movement) {
            this.movement.measures.splice(this.movement.measures.indexOf(this));
        }*/
        this.movement = movement;
       // this.movement.measures.push(this);
    }

    public unbindFromMovement() {
       // this.movement.measures.splice(this.movement.measures.indexOf(this), 1);
        this.movement = 0;
    }

    public getMovementOrdinal(): number {
        return this.movement;
        
    }

   

    public static compareByOrdinals(a: Measure, b: Measure) {
        if (a.group && b.group) {
            if (a.group.ordinal !== b.group.ordinal) {
                return a.group.ordinal - b.group.ordinal;
            }
            else {
                if (a.parent && b.parent) {
                    if (a.parent.ordinal !== b.parent.ordinal) {
                        return a.parent.ordinal - b.parent.ordinal;
                    }
                    else {
                        return a.ordinal - b.ordinal;
                    }
                }
                else {
                    if (a.parent) {
                        return a.ordinal - b.parent.ordinal;
                    }
                    if (b.parent) {
                        return a.parent.ordinal - b.parent.ordinal;
                    }
                    return a.ordinal - b.ordinal;
                }
            }
        }
        else {
            if (a.group && !b.group) {
                if (b.parent) {
                    return a.group.ordinal - b.parent.ordinal;
                }
                return a.group.ordinal - b.ordinal;
            } else
                if (b.group && !a.group) {
                    if (a.parent) {
                        return a.parent.ordinal - b.group.ordinal;
                    }
                    return a.ordinal - b.group.ordinal;
                } else {
                    if (a.parent && b.parent) {
                        if (a.parent.ordinal !== b.parent.ordinal) {
                            return a.parent.ordinal - b.parent.ordinal;
                        }
                        else {
                            return a.ordinal - b.ordinal;
                        }
                    }
                    else {
                        if (a.parent) {
                            return a.ordinal - b.parent.ordinal;
                        }
                        if (b.parent) {
                            return a.parent.ordinal - b.parent.ordinal;
                        }
                        return a.ordinal - b.ordinal;
                    }
                }
        }

    }

    public getMeasureName(): string {
        var result = '';
        if (this.group !== null) {
            result += 'G' + this.group.ordinal + '.';
        }
        if (this.parent !== null) {
            result += this.parent.ordinal + '.';
        }
        if (this.ordinal) {
            result += this.ordinal + this.suffix;
        }
        else {
            result += '(undefined)' + this.suffix;
        }
        return result;
    }

    public predictMeasureNumber() {
        var movement = this.getMovementOrdinal();

    /*    if (movement.measures.length == 1) {
            this.ordinal = 1;
            return;
        }
        if (this.parent != null) {
            this.ordinal = _.max(_.pluck(_.where(movement.measures, { parent: this.parent }), 'ordinal')) + 1;
        } else
            if (this.group != null) {
                this.ordinal = _.max(_.pluck(_.where(movement.measures, { group: this.group }), 'ordinal')) + 1;
            } else {
                this.ordinal = _.max(_.union(_.pluck(movement.measures, 'ordinal'),
                    _.pluck(_.pluck(movement.measures, 'group'), 'ordinal'))) + 1;
            }*/
    }
}