﻿import { Measure } from './measure';
//import { Movement } from './movement';

export class ItemPage {
    id: string;
    status: string;
    imgPath: string;
    measures: Measure[];
    width: number;
    height: number;

    constructor(id: string, imgPath: string, status?: string, width?: number, height?: number, measures?: Measure[]) {
        this.id = id;      
        this.imgPath = imgPath;
        this.status = status || "unknown";
        this.width = width || 0;
        this.height = height || 0;
        this.measures = measures || [];
    }
}