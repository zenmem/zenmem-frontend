﻿//import { Measure } from './measure';

export class MeasurePart {
   // measures: Measure[];
    id: number;
    ordinal: number;
    vertexes: [number, number][];
    midpoints: [number, number][];
    parent: MeasurePart;
    suffix: string;
    visible: boolean;
    pause: number;
    upbeat: boolean;
    excluded: boolean;
    locked: boolean;
    rectangle: boolean;


    constructor(id: number, ordinal: number, suffix?: string, visible?: boolean, pause?: number, upbeat?: boolean, excluded?: boolean, vertexes?: [number, number][], locked?: boolean,
        midpoints?: [number, number][], parent?: MeasurePart, rectangle?: boolean) {
        this.id = id;
        this.ordinal = ordinal;
        this.vertexes = vertexes || [];
        this.midpoints = midpoints || [];
        this.parent = parent || null;
        this.suffix = suffix || "";
        this.visible = visible || true;
        this.excluded = excluded || false;
        this.upbeat = upbeat || false;
        this.locked = locked || false;
        this.pause = pause || 0;
        this.rectangle = rectangle || false;

      
    }

    public static compareByOrdinals(a: MeasurePart, b: MeasurePart) {
        if (a && b) {
            if (a.ordinal !== b.ordinal) {
                return a.ordinal - b.ordinal;
            }
            else {
                if (a.parent && b.parent) {
                    if (a.parent.ordinal !== b.parent.ordinal) {
                        return a.parent.ordinal - b.parent.ordinal;
                    }
                    else {
                        return a.ordinal - b.ordinal;
                    }
                }
                else {
                    if (a.parent) {
                        return a.ordinal - b.parent.ordinal;
                    }
                    if (b.parent) {
                        return a.parent.ordinal - b.parent.ordinal;
                    }
                    return a.ordinal - b.ordinal;
                }
            }
        }
        else {
            if (a && !b) {
                if (b.parent) {
                    return a.ordinal - b.parent.ordinal;
                }
                return a.ordinal - b.ordinal;
            } else
                if (b && !a) {
                    if (a.parent) {
                        return a.parent.ordinal - b.ordinal;
                    }
                    return a.ordinal - b.ordinal;
                } else {
                    if (a.parent && b.parent) {
                        if (a.parent.ordinal !== b.parent.ordinal) {
                            return a.parent.ordinal - b.parent.ordinal;
                        }
                        else {
                            return a.ordinal - b.ordinal;
                        }
                    }
                    else {
                        if (a.parent) {
                            return a.ordinal - b.parent.ordinal;
                        }
                        if (b.parent) {
                            return a.parent.ordinal - b.parent.ordinal;
                        }
                        return a.ordinal - b.ordinal;
                    }
                }
        }

    }

    public getMeasureName(): string {
        var result = '';
        if (this !== null) {
            result += 'G' + this.ordinal + '.';
        }
        if (this.parent !== null) {
            result += this.parent.ordinal + '.';
        }
        if (this.ordinal) {
            result += this.ordinal + this.suffix;
        }
        else {
            result += '(undefined)' + this.suffix;
        }
        return result;
    }

}