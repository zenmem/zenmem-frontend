﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ButtonsModule } from 'ng2-bootstrap/ng2-bootstrap';
import { DropdownModule } from 'ng2-bootstrap/ng2-bootstrap';
import { TooltipModule } from 'ng2-bootstrap/ng2-bootstrap';

import { MEditorComponent } from './meditor.component';
import { MeasureComponent } from './measure.component';
import { MeasureNamePipe } from './pipes/measureName.pipe';
import { PaginatorPipe } from './pipes/paginator.pipe';
import { MovementOrdinalPipe } from './pipes/movementOrdinal.pipe';
import { MeasuresOnPagePipe } from './pipes/measuresOnPage.pipe';
import { ModeCreationIconPipe } from './pipes/modeCreationIcon.pipe';
import { ModeModificationIconPipe } from './pipes/modeModificationIcon.pipe';

//CollapseDirective ???

/**
 * Do not specify providers for modules that might be imported by a lazy loaded module.
 */

@NgModule({
    imports: [CommonModule, RouterModule, FormsModule, ButtonsModule, DropdownModule, TooltipModule],
    declarations: [MEditorComponent, MeasureComponent, MeasureNamePipe, PaginatorPipe, MovementOrdinalPipe, MeasuresOnPagePipe, ModeCreationIconPipe, ModeModificationIconPipe],
    exports: [MEditorComponent, MeasureComponent, CommonModule, FormsModule, RouterModule, ButtonsModule, DropdownModule, TooltipModule]
})
export class MEditorModule {
}
