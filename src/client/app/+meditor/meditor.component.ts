﻿import { Component, ViewChild } from '@angular/core';
import { Item } from './types/item';
import { ItemPage } from './types/itemPage';
import { Measure } from './types/measure';
//import { Movement } from './types/movement';
import { MeasureGroup } from './types/measureGroup';
import { MeasureComponent } from './measure.component';

declare var SwaggerClient: any;

@Component({
    moduleId: module.id,
    selector: "meditor",
    templateUrl: "meditor.html"

})
export class MEditorComponent {
    item: Item;
    itemPageIndex: number = 0;
    //sidePanelsColapsed: boolean;
    mode: string = 'rect';
    explorer: string = 'MEASURES'
    @ViewChild(MeasureComponent) measuresCmp: MeasureComponent;
    selectedMeasures: Measure[] = [];

    swClient: any;

    onSelectionChanged(selection: Measure[]): void {
        this.selectedMeasures = selection;
    }

    constructor() {
        var that = this;
        /*this.swClient = new SwaggerClient({
            url: 'http://localhost:5000/swagger/v1/swagger.json',
            success: function () {
                that.swClient.FacsimileApi.FacsimileBysourceSourceidGet({ sourceid: 0 }, { responseContentType: 'application/json' }, function (facsimile: any) {
                    console.log('facsimile', facsimile);
                });
            }
        });*/

        var itemPages: ItemPage[] = [];
        itemPages.push(new ItemPage("1", 'assets/summer_school/Brahms/p1.jpg', "unknown" ,3188, 2578));
        itemPages.push(new ItemPage("2", 'assets/summer_school/Brahms/p2.jpg', "unknown", 3188, 2578));
        itemPages.push(new ItemPage("3", 'assets/summer_school/Brahms/11.jpg', "unknown", 3908, 2880));
        itemPages.push(new ItemPage("4", 'assets/summer_school/Weber/p9.jpg', "unknown", 3908, 2880));
        itemPages.push(new ItemPage("5", 'assets/summer_school/Weber/p10.jpg', "unknown", 3908, 2880));
        itemPages.push(new ItemPage("6", 'assets/summer_school/Weber/p11.jpg', "unknown", 3908, 2880));
        //itemPages.forEach(ip => ip.movements.push(new Movement(1)));
        this.item = new Item("01", "Brahms and Weber", "Edirom Summer School Edition", "Parts for UI-evaluation", "unknown", itemPages);
        //Nastja this.item.movements.push(new Movement(1));

        //this.sidePanelsColapsed = false;

        //this.item.pages[0].measures.push(new Measure(1, [[30, 100],
        //    [300, 100], [300, 400], [30, 400]]));
        //this.item.pages[0].measures.push(new Measure(2, [[250, 100],
        //    [250, 400], [500, 400], [500, 100]], "A"));
        //this.item.pages[0].measures.push(new Measure(3, [[450, 100],
        //   [450, 400], [620, 400], [620, 100]]));
        //this.item.pages[0].measures.push(new Measure(1, [[450, 120],
        //    [450, 200], [620, 200], [620, 120]], null, this.item.pages[0].measures[2]));
        //this.item.pages[0].measures.push(new Measure(2, [[450, 270],
        //    [450, 380], [620, 380], [620, 270]], null, this.item.pages[0].measures[2]));
        //var group: MeasureGroup = new MeasureGroup(4);
        //this.item.pages[0].measures.push(new Measure(1, [[600, 100],
        //    [600, 400], [750, 400], [750, 100]], null, null, group));
        //this.item.pages[0].measures.push(new Measure(2, [[730, 100],
        //    [730, 400], [880, 400], [880, 100]], null, null, group));
        //this.item.pages[0].measures.push(new Measure(3, [[730, 420],
        //    [730, 720], [880, 720], [880, 420]], "NK", this.item.pages[0].measures[4], group));

    }

    removeMeasures() {
        this.measuresCmp.removeMeasures();
    }

    lock() {
        this.measuresCmp.lock();
    }

    unlock() {
        this.measuresCmp.unlock();
    }

    convertToRectangle() {
        this.measuresCmp.convertToRectangle();
    }

    moveToBack() {
        this.measuresCmp.moveToBack();
    }

    selectMeasures(measures: Measure[]) {
        this.measuresCmp.selectMeasures(measures);
    }

    selectionContainsLocked() {
        return this.selectedMeasures.map(function (obj) { return obj.locked }).includes(true);
    }

    selectionContainsUnlocked() {
        return this.selectedMeasures.map(function (obj) { return obj.locked }).includes(false);
    }

    selectionExists() {
        return this.selectedMeasures.length > 0;
    }

    refresh() {
        this.measuresCmp.redraw();
    }

    pageDecrement() {
        if (this.itemPageIndex > 0) this.itemPageIndex--;
    }

    pageIncrement() {
        if (this.itemPageIndex < this.item.pages.length - 1) this.itemPageIndex++;
    }

    decrementSelected() {
        var that = this;
        if (this.selectedMeasures.map(function (obj) { return obj.ordinal; }).includes(1)) { return; }
        this.selectedMeasures.forEach(function (obj) {
            that.item.pages[that.itemPageIndex].measures[
                that.item.pages[that.itemPageIndex].measures.indexOf(obj)
            ].ordinal--;
        });
        this.refresh();
    }

    incrementSelected() {
        var that = this;
        this.selectedMeasures.forEach(function (obj) {
            that.item.pages[that.itemPageIndex].measures[
                that.item.pages[that.itemPageIndex].measures.indexOf(obj)
            ].ordinal++;
        });
        this.refresh();
    }

    /*createMovement() {
        var that = this;
        this.item.pages[that.itemPageIndex].measures[
            that.item.pages[that.itemPageIndex].measures.indexOf(obj)
        ].movement++;
       
    }

    removeLastMovement() {
        if (this.item.movements[this.item.movements.length - 1]
            .measures.length === 0 && this.item.movements.length > 1) {
            this.item.movements.pop();
            this.item.currentMovement--;
        }
    }*/

   /* changeMovement(index: number) {
        this.item.pages[this.itemPageIndex].measures[this.item.pages[this.itemPageIndex]
            .measures.indexOf(this.selectedMeasures[0])].bindToMovement(this.item.movements[index]);
    }*/
}