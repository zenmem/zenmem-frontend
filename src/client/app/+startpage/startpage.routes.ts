﻿import { StartpageComponent } from './startpage.component';

export const StartpageRoutes = [
    { path: 'startpage', component: StartpageComponent, index: true } // ''
];
