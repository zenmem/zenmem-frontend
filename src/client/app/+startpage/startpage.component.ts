﻿import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: "startpage",
    templateUrl: "startpage.html"
})
export class StartpageComponent { }