﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { StartpageComponent } from './startpage.component';

/**
 * Do not specify providers for modules that might be imported by a lazy loaded module.
 */

@NgModule({
    imports: [CommonModule, RouterModule],
    declarations: [StartpageComponent],
    exports: [StartpageComponent, CommonModule, FormsModule, RouterModule]
})
export class StartpageModule {
}
