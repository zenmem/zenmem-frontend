import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { APP_BASE_HREF } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { routes } from './app.routes';

//import { AboutModule } from './about/about.module';
//import { HomeModule } from './home/home.module';
import { ToolsOverviewModule } from './+toolsoverview/toolsoverview.module';
import { SEditorModule } from './+seditor/seditor.module';
import { SharedModule } from './shared/shared.module';
import { StartpageModule } from './+startpage/startpage.module'
import { MEditorModule } from './+meditor/meditor.module';

@NgModule({
    imports: [MEditorModule, StartpageModule, SEditorModule, ToolsOverviewModule, BrowserModule, HttpModule, RouterModule.forRoot(routes), SharedModule.forRoot()],
    declarations: [AppComponent],
    providers: [{
    provide: APP_BASE_HREF,
    useValue: '<%= APP_BASE %>'
  }],
  bootstrap: [AppComponent]

})

export class AppModule { }
